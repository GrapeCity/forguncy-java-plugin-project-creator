package com.grapecity.forguncy.Entity;

import java.util.regex.Pattern;

public class Version {
    public String majorVersion;
    public String minorVersion;
    public String patchVersion;
    public String buildVersion;

    public Version(String versionString) {
        // 定义版本号的正则表达式
        String regex = "^(\\d+)\\.(\\d+)\\.(\\d+)\\.(\\d+)$";
        var pattern = Pattern.compile(regex);
        var matcher = pattern.matcher(versionString);

        if (matcher.matches()) {
            majorVersion = matcher.group(1);
            minorVersion = matcher.group(2);
            patchVersion = matcher.group(3);
            buildVersion = matcher.group(4);
        }
    }
}
