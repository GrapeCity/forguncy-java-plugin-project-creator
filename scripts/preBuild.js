import fs from 'fs';
import path from 'path';
import {rimraf} from 'rimraf'

const __dirname = process.cwd();
const folderPath = path.join(__dirname, '../src-tauri/target/release')
// 清空文件夹
if (fs.existsSync(folderPath)) {
    fs.readdir(folderPath, (err, files) => {
        if (err) {
            console.error('读取文件夹时出现错误:', err);
            return;
        }
        // 遍历文件夹中的文件和子文件夹
        for (const file of files) {
            const filePath = path.join(folderPath, file);
            // 删除文件或递归清空子文件夹
            rimraf.sync(filePath);
        }

        console.log('文件夹已清空');
    });
}