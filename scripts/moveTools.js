import fs from 'fs';

function moveFile(sourcePath, destinationPath) {
  fs.cpSync(sourcePath, destinationPath);
}

movePackageZip();
moveAutoInstall();
function movePackageZip() {
    const sourceFile = './AutoGenerateMetaInfo/target/packageZip_jwd.jar';
    const destinationFile = './src-tauri/packageTools/packageZip_jwd.jar';

    moveFile(sourceFile, destinationFile);
}

function moveAutoInstall() {
    const sourceFile = './commandPluginAutoinstaller/target/autoInstall_jwd.jar';
    const destinationFile = './src-tauri/packageTools/autoInstall_jwd.jar';
    moveFile(sourceFile, destinationFile);
}
