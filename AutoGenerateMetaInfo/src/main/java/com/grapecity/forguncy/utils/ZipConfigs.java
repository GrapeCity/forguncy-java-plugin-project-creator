package com.grapecity.forguncy.utils;

import java.util.ArrayList;

public class ZipConfigs {
    public String path;
    // 竖线分开
    public String excludeFileNames = "";
    public boolean packageToRoot;

    public ArrayList<String> includeAbsPath;

    public boolean ignoreFirstLayer = false;

    public ZipConfigs(String path) {
        this.path = path;
    }

    public ZipConfigs(String path, String excludeFileNames) {
        this.path = path;
        this.excludeFileNames = excludeFileNames;
    }

    public ZipConfigs(String path, String excludeFileNames, Boolean packageToRoot) {
        this.path = path;
        this.excludeFileNames = excludeFileNames;
        this.packageToRoot = packageToRoot;
    }

    public ZipConfigs(String path, String excludeFileNames, Boolean packageToRoot,
                      ArrayList<String> includeRelativePath, boolean ignoreFirstLayer) {
        this.path = path;
        this.excludeFileNames = excludeFileNames;
        this.packageToRoot = packageToRoot;
        this.includeAbsPath = includeRelativePath;
        this.ignoreFirstLayer = ignoreFirstLayer;
    }
}
