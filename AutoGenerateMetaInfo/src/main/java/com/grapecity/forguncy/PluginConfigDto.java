package com.grapecity.forguncy;

public class PluginConfigDto {
    public String[] assembly;
    public String[] javascript;
    public String[] css;
    public String[] jar;
    public String image;
    public String description;
    public String description_cn;
    public String name;
    public String name_cn;
    public String pluginType;
    public String guid;
    public String version;
    public String dependenceVersion;
}
