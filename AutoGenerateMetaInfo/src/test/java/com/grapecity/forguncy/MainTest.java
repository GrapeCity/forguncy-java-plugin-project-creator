package com.grapecity.forguncy;

import junit.framework.TestCase;
import org.junit.Assert;

import java.io.File;
import java.io.IOException;


public class MainTest extends TestCase {

    public void testPackageWebApi() throws IOException {
        String path = "src\\test\\java\\com\\grapecity\\forguncy\\Resoucres\\forguncy-java-api\\TestBaseApi\\TestBaseApi";
        String metaPath = path + "\\target\\com.grapecity.forguncy.serverapi.entity.ForguncyApi";
        String zipPath = path + "\\target\\test-base-api-1.0.0-jar-with-dependencies.fgcjwa.zip";
        removeWebApiDist(zipPath, metaPath);
        Main.packageWebApi(getAbsolutePath(path));
        Assert.assertTrue(new File(getAbsolutePath(metaPath)).exists());
        Assert.assertTrue(new File(getAbsolutePath(zipPath)).exists());
        removeWebApiDist(zipPath, metaPath);
    }

    private static void removeWebApiDist(String zipPath, String metaPath) {
        removeIfExist(getAbsolutePath(zipPath));
        removeIfExist(getAbsolutePath(metaPath));
    }

    public void testPackageSecurityProvider() throws IOException {
        String path = "src\\test\\java\\com\\grapecity\\forguncy\\Resoucres\\forguncy-java-security-provider\\TestBaseSecurityProvider\\TestBaseSecurityProvider";
        String metaPath = path + "\\target\\com.grapecity.forguncy.securityprovider.ISecurityProvider";
        String zipPath = path + "\\target\\test-base-security-provider-1.0.0-jar-with-dependencies.zip";
        removeSPDist(zipPath, metaPath);
        Main.packageSecurityProvider(getAbsolutePath(path), new String[0]);
        Assert.assertTrue(new File(getAbsolutePath(metaPath)).exists());
        Assert.assertTrue(new File(getAbsolutePath(zipPath)).exists());
        removeSPDist(zipPath, metaPath);
    }

    private static void removeSPDist(String zipPath, String metaPath) {
        removeIfExist(getAbsolutePath(zipPath));
        removeIfExist(getAbsolutePath(metaPath));
    }

    public void testPackagePlugin() throws IOException {
        String path = "src\\test\\java\\com\\grapecity\\forguncy\\Resoucres\\forguncy-java-plugin\\TestBasePlugin\\TestBasePlugin";
        String metaPath = path + "\\target\\com.grapecity.forguncy.commands.entity.Command";
        String zipPath = path + "\\target\\test-base-plugin-command-1.0.0-jar-with-dependencies.zip";
        String pluginConfigPath = path + "\\target\\PluginConfig.json";
        removePluginDist(zipPath, metaPath, pluginConfigPath);
        Main.packagePlugin(getAbsolutePath(path));
        Assert.assertTrue(new File(getAbsolutePath(metaPath)).exists());
        Assert.assertTrue(new File(getAbsolutePath(zipPath)).exists());
        Assert.assertTrue(new File(getAbsolutePath(pluginConfigPath)).exists());
        removePluginDist(zipPath, metaPath, pluginConfigPath);
    }

    private static void removePluginDist(String zipPath, String metaPath, String pluginConfigPath) {
        removeIfExist(getAbsolutePath(zipPath));
        removeIfExist(getAbsolutePath(metaPath));
        removeIfExist(getAbsolutePath(pluginConfigPath));
    }

    private static String getAbsolutePath(String path) {
        File file = new File(path);
        return file.getAbsolutePath();
    }
    public static void removeIfExist(String filePath) {
        File file = new File(filePath);
        if (file.exists()) {
            file.delete();
        }
    }
}