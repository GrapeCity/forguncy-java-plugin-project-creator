package org.example;

import lombok.Data;

@Data
public class Config {
    public String ak;
    public String sk;
}
