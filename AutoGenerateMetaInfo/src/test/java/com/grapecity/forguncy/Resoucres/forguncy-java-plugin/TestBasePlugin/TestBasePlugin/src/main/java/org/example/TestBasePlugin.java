package org.example;

import com.grapecity.forguncy.commands.ICommandExecutableInServerSide;
import com.grapecity.forguncy.commands.IServerCommandExecuteContext;
import com.grapecity.forguncy.commands.annotation.ResultToProperty;
import com.grapecity.forguncy.commands.entity.Command;
import com.grapecity.forguncy.commands.entity.ExecuteResult;
import com.grapecity.forguncy.commands.enumeration.CommandScope;
import com.grapecity.forguncy.plugincommon.common.annotation.*;
import lombok.Data;

@Data
@Icon("resources/Icon.png")
public class TestBasePlugin extends Command implements ICommandExecutableInServerSide {
    @DisplayName("名称:")
    @FormulaProperty
    @Required
    private Object name = "TestBasePlugin";

    @Required
    @FormulaProperty
    @DisplayName("加数1")
    private Object addNumber1;

    @Required
    @FormulaProperty
    @DisplayName("加数2")
    private Object addNumber2;

    @ResultToProperty
    @FormulaProperty
    @DisplayName("相加结果")
    private String resultTo = "结果";

    /**
     * 命令执行方法
     * @param dataContext 从服务器端传递给命令执行时的上下文信息
     * @return 是否执行成功
     */
    @Override
    public ExecuteResult execute(IServerCommandExecuteContext dataContext) {

        int add1num = Integer.parseInt(addNumber1.toString());
        int add2num = Integer.parseInt(addNumber2.toString());

        dataContext.getParameters().put(resultTo, add1num + add2num);

        ExecuteResult executeResult = new ExecuteResult();
        executeResult.getReturnValues().put("hello", name);
        return executeResult;
    }

    /**
     * 返回插件的名称
     * @return 插件的名称
     */
    @Override
    public String toString() {
        return "我的插件";
    }
}
