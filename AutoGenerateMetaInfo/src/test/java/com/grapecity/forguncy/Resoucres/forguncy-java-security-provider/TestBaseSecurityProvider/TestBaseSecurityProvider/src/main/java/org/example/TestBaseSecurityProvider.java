package org.example;

import com.grapecity.forguncy.securityprovider.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

/**
 * 一个简单的安全提供程序样例
 */
public class TestBaseSecurityProvider implements ISecurityProvider {

    /**
     * 第三方系统中的所有用户信息，如果不需要提供用户信息，则返回 null
     * 
     * @return 第三方系统中的所有用户信息
     */
    @Override
    public UserInformations getUserInformations() {
        UserInformations userInformations = new UserInformations();
        HashSet<User> users = userInformations.getUsers();
        User user = new User();
        user.setUserId("ForguncyTest");
        user.setEmail("test@example.com");
        users.add(user);

        HashSet<Role> roles = userInformations.getRoles();
        Role role = new Role();
        role.setName("TestRole");
        role.getUsers().add(user);
        roles.add(role);

        List<Organization> organizations = userInformations.getOrganizations();
        Organization orgB = new Organization();
        orgB.setName("orgB");
        orgB.setOrganizationLevel("Level2");
        HashSet<OrganizationMember> orgBMember = orgB.getMembers();
        OrganizationMember organizationMemberB = new OrganizationMember();
        organizationMemberB.setUser(user);
        var rolesInOrg = new ArrayList<Role>();
        rolesInOrg.add(role);
        organizationMemberB.setOrganizationRoles(rolesInOrg);
        organizationMemberB.setIsLeader(false);
        orgBMember.add(organizationMemberB);
        organizations.add(orgB);
        return userInformations;
    }

    /**
     * 认证用户，在登录活字格时会调用此方法
     * 
     * @param properties 所需用户相关信息的参数字典，此例中，用户名以及密码会从此参数中获取
     * @return 认证成功则返回用户信息，否则可以返回 null 表示用户认证失败
     */
    @Override
    public User verifyUser(HashMap<String, String> properties) {
        String name = properties.get("userName");
        for (User user : getUserInformations().getUsers()) {
            if (user.getUserId().equals(name)) {
                return user;
            }
        }
        return null;
    }

    /**
     * 安全提供程序名称，主要用于在服务器中展示
     * 
     * @return 安全提供程序名称
     */
    @Override
    public String getName() {// 返回securityProvider的名称（中文）
        return "TestBaseSecurityProvider";
    }

    /**
     * 用户认证的类型，目前默认为 UserNameAndPassword 即账号密码认证
     * 此例中也以这种方式进行认证
     * 
     * @return 用户认证的类型
     */
    @Override
    public AuthenticationType getAuthenticationType() {
        return AuthenticationType.UserNameAndPassword;
    }

    /**
     * 第三方应用获取的数据如何在活字格中保存，目前默认为 InForguncyDatabase 即保存在活字格数据库中
     * 此例中也以这种方式进行保存
     * 
     * @return 第三方应用获取的数据如何在活字格中保存
     */
    @Override
    public UserInformationStorageMode getUserInformationStorageMode() {
        return UserInformationStorageMode.InMemoryCache;
    }

    /**
     * 是否支持登出操作，如果支持，则在活字格中可以执行登出操作
     * 
     * @return 是否支持登出操作
     */
    @Override
    public boolean getAllowLogout() {
        return true;
    }

}