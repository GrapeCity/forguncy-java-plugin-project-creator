import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App";
import "./styles.css";
import { initializeIcons } from '@fluentui/react/lib/Icons';
import { GlobalContextProvider } from "./Components/Context/global";
initializeIcons(/* optional base url */);
ReactDOM.createRoot(document.getElementById("root") as HTMLElement).render(
  <React.StrictMode>
    <GlobalContextProvider>
      <App />
    </GlobalContextProvider>
  </React.StrictMode>,
);
