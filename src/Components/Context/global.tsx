import React from 'react';
import { isDev } from '../utils';

export interface GlobalContextType {
    errorMsg: string,
    createSuccess: string,
    forguncyPath:string,
}

const initValue = isDev() ? "D:\\dev\\forguncy" : "C:\\Program Files\\Forguncy 10"
export const GlobalContextDefaultValue: GlobalContextType = {
    errorMsg: '',
    createSuccess: '',
    forguncyPath:initValue
};
export const GlobalContext = React.createContext<{
    context: GlobalContextType;
    dispatch?: (action: { type: keyof GlobalContextType, payload: any }) => void
}>({ context: GlobalContextDefaultValue });

function reducer(state: GlobalContextType, action: {
    type: keyof GlobalContextType;
    payload: any;
}): GlobalContextType {
    return { ...state, [action.type]: action.payload };
}

export const GlobalContextProvider = (props: any) => {
    const [value, dispatch] = React.useReducer(reducer, GlobalContextDefaultValue);
    return <GlobalContext.Provider value={{ context: value, dispatch: dispatch }}>
        {props.children}
    </GlobalContext.Provider>;
};