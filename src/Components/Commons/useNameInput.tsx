import { useState } from "react";
import { isUpperCamelCase } from "../utils";
import { MessageBar, MessageBarType, TextField } from "@fluentui/react";
import { RS } from "../../i18n/config";

interface IProps {
    label: string;
    initValue: string;
}
function useNameInput(props: IProps) {
    const [name, setName] = useState(props.initValue);
    const nameValid = name.length > 0 && isUpperCamelCase(name);
    const dom = <>
        <TextField
            maxLength={50}
            errorMessage={nameValid ? undefined : RS.Name_Must_Be_CamelCase}
            label={props.label + RS.Name_Label}
            value={name}
            onChange={ev => setName((ev.target as HTMLInputElement).value)}
            required
            placeholder={props.initValue}
        />

        {(!(nameValid)) ? undefined : <MessageBar
            messageBarType={MessageBarType.success}
            isMultiline={true}
        >
            {props.label} {RS.Name_Valid}
        </MessageBar>}</>;

    return [dom, name, nameValid] as const;
}
export default useNameInput;