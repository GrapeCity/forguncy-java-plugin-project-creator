import { MessageBar, MessageBarType, TextField } from "@fluentui/react";
import { useState } from "react";
import { RS } from "../../i18n/config";

export const defaultPackageClass = "org.example";
export default function usePackageClassInput() {
    const [packageClass, setpackageClass] = useState(defaultPackageClass);
    const packageNameIsValid = isValidPackageName(packageClass || defaultPackageClass);
    const Dom = <><TextField
        maxLength={50}
        errorMessage={packageNameIsValid ? undefined : RS.Package_Name_Not_Valid}
        label={RS.Package_Name}
        value={packageClass}
        onChange={ev => setpackageClass((ev.target as HTMLInputElement).value)}
        placeholder={defaultPackageClass}
    />
        {
            packageNameIsValid ? <MessageBar
                messageBarType={MessageBarType.success}
                isMultiline={true}
            >
                {RS.Package_Name_Valid}
            </MessageBar> : ""
        }</>;
    return [Dom, (packageClass || defaultPackageClass), packageNameIsValid] as const;
}

const packageReg = /(?:^\w+|\w+\.\w+)+$/;

function isValidPackageName(name: string) {
    return packageReg.test(name);
}