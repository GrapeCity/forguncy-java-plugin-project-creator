import En from './resources/en';

export type Translation = typeof En;