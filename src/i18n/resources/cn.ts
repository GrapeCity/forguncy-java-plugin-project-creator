
import { Translation } from "../JsonType";

const cn: Translation = {
    "Forguncy_Creator":"活字格Java扩展创建工具",
    "Web_Api":"Web Api",
    "Security_Provider":"安全提供程序",
    "Plugin":"服务端命令插件",
    "Create_Success":"创建项目成功",
    "Confirm":"确定",
    "Open_Folder":"打开文件夹",
    "Forguncy_Install_Path":"活字格设计器安装路径",
    "Forguncy_Java_plugin_support_file_not_found":"未找到活字格Java 插件支持文件",
    "Select":"选择",
    "Found_Support_file_version":"找到{{- a}}支持文件版本:{{- b}}",
    "Name_Must_Be_CamelCase":"名称不能为空且必须为大驼峰命名",
    "Name_Label":"名称（使用大驼峰命名）",
    "Name_Valid":"名称合法",
    "Package_Name_Not_Valid":"包名不合法",
    "Package_Name_Valid":"包名合法",
    "Package_Name":"包名",
    "Save_to_Folder":"保存{{- a}}至文件夹",
    "Path_Not_Valid":"不是合法路径",
    "Plugin_Label":"插件",
    "descDefault":"这是一个活字格插件",
    "Plguin_CN_Name":"插件中文名称",
    "Plguin_Desc":"插件描述",
    "Create_Plugin":"创建服务端命令插件",
    "Project_Will_Be_Created_In":"项目将创建在：",
    "Create_Security_Provider":"创建安全提供程序",
    "Create_Web_Api":"创建 Web Api",
    "Folder_Not_Empty":"目标文件夹不为空,请重新指定目录",
    "None_En_Path_Warn_A":"目标路径含有非英文字符，在一些情况下可能会无法正常编译插件，推荐生成路径只包含英文字符。",
    "None_En_Path_Warn_B":"您也可以在遇到编译问题时尝试移动生成的项目到英文路径下。"
    
}
export default cn;