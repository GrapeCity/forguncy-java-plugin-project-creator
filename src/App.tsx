
import { Dialog, DialogType, Icon, MessageBar, MessageBarType, Modal, Pivot, PivotItem, PrimaryButton, DefaultButton } from '@fluentui/react';
import WebApi from './Components/WebApi/Index';
import SecurityProvider from './Components/SecurityProvider';
import Plugin from './Components/Plugin';
import { useId } from '@fluentui/react-hooks';
import { useContext, useEffect, useState } from 'react';
import { GlobalContext } from './Components/Context/global';
import { contentStyles } from './Components/utils';
import React from 'react';
import { Command } from '@tauri-apps/api/shell';
import initI18n, { RS, tk } from './i18n/config';
import { appWindow } from '@tauri-apps/api/window';

interface IProps {

}
const dialogStyles = { main: { maxWidth: 450 } };
function App(_props: IProps) {
  const titleId = useId("title");
  const labelId: string = useId('dialogLabel');
  const subTextId: string = useId('subTextLabel');
  const [loaded, setLoaded] = useState(false);
  useEffect(() => {
    initI18n().then(() => {
      setLoaded(true);
      appWindow.setTitle(RS.Forguncy_Creator);
    });
    document.addEventListener('contextmenu', function (event) {
      event.preventDefault();
    });
  }, [])
  const modalProps = React.useMemo(
    () => ({
      titleAriaId: labelId,
      subtitleAriaId: subTextId,
      isBlocking: false,
      styles: dialogStyles,
    }),
    [labelId, subTextId],
  );
  const { context: { errorMsg, createSuccess }, dispatch } = useContext(GlobalContext);
  const [selectedKey, setSelectKey] = useState(tk.Web_Api);
  if (!loaded) {
    return <></>
  }
  return <>
    <h3 style={{ textAlign: "center" }}>{RS.Forguncy_Creator}</h3>
    <Pivot selectedKey={selectedKey} onLinkClick={(item) => {
      if (item?.props.itemKey) {
        setSelectKey(item.props.itemKey)
      }
    }}>
      <PivotItem itemKey={tk.Web_Api} headerText={RS.Web_Api}>
      </PivotItem>
      <PivotItem itemKey={tk.Security_Provider} headerText={RS.Security_Provider}>
      </PivotItem>
      <PivotItem itemKey={tk.Plugin} headerText={RS.Plugin}>
      </PivotItem>
    </Pivot>
    <div style={{ display: selectedKey === tk.Web_Api ? 'block' : 'none' }}>
      <WebApi />
    </div>
    <div style={{ display: selectedKey === tk.Security_Provider ? 'block' : 'none' }}>
      <SecurityProvider />
    </div>
    <div style={{ display: selectedKey === tk.Plugin ? 'block' : 'none' }}>
      <Plugin />
    </div>

    <Modal
      titleAriaId={titleId}
      isOpen={!!errorMsg}
      onDismiss={() => {
        dispatch?.({ type: 'errorMsg', payload: '' });
      }}
      isBlocking={false}
      containerClassName={contentStyles.container}
    >
      <MessageBar
        messageBarType={MessageBarType.error}
        isMultiline={true}
        dismissButtonAriaLabel="Close"
      >
        {errorMsg}
      </MessageBar>
    </Modal>

    <Dialog
      hidden={!createSuccess}
      onDismiss={() => {
        dispatch?.({ type: 'createSuccess', payload: '' })
      }}
      dialogContentProps={{
        type: DialogType.normal,
        title: <div></div>
      }}
      modalProps={modalProps}
    >
      <div style={{ textAlign: 'center' }}>
        <Icon style={{ fontSize: '48px', color: 'rgb(16, 124, 16)' }} iconName="Completed" />
        <h3>{RS.Create_Success}</h3>
      </div>
      <div style={{ display: 'flex', justifyContent: 'space-around', alignItems: 'center' }}>
        <DefaultButton onClick={() => {
          dispatch?.({ type: 'createSuccess', payload: '' })
        }} text={RS.Confirm} />
        <PrimaryButton onClick={() => {
          new Command("run-exp", createSuccess).execute();
        }} text={RS.Open_Folder} />
      </div>
    </Dialog>
  </>
}
export default App;